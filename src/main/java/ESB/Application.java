package ESB;

import ESB.Configuration.TrustStore;
import ESB.Controller.VFS.VFSStarter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
            VFSStarter.initVFSListeners();
        } catch (Exception exception) {
            logger.error("Exception: " + exception.getMessage());
        }
    }
}