package ESB.Controller.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionController {

    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ExceptionInfo handleIllegalArgument(Exception ex) {
        return new ExceptionInfo(ex);
    }
}
