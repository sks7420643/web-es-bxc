package ESB.Controller.Exception;

public class ExceptionInfo {
    public final String error;

    public ExceptionInfo(Exception exception) {
        this.error = exception.getLocalizedMessage();
    }
}
