package ESB.Controller.VFS;

import java.util.EventListener;

public interface VFSListener extends EventListener {
    void onCreated(FileEvent event);
}
