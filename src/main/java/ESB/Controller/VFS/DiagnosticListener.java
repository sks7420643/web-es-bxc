package ESB.Controller.VFS;

import jakarta.servlet.annotation.WebListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.nio.file.WatchService;

@WebListener
public class DiagnosticListener implements ServletContextListener {

    private static final Logger logger = LoggerFactory.getLogger(DiagnosticListener.class);

    @Override
    public void contextInitialized( final ServletContextEvent sce ) {
        logger.info("Context initialized.");
        }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        logger.info("Context destroyed.");
        for (WatchService watchService : VFSWatcher.getWatchServices()) {
            try {
                watchService.close();
            } catch (IOException e) {
                logger.error("VFS closing fail: " + e.getMessage());
            }
        }
    }
}