package ESB.Controller.Security;

import ESB.Repository.Security.Vault.VaultSecret;
import ESB.Service.Security.VaultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Vault")
public class VaultController {

    @Autowired
    private VaultService vaultService;

    @PutMapping("/PutSecret")
    String putSecret(@RequestBody VaultSecret vaultSecret) {
        return vaultService.addSecret(vaultSecret);
    }

    @GetMapping("/GetSecret/{key}")
    String requestSecret(@PathVariable String key) {
        return vaultService.getSecret(key);
    }

    @PutMapping("/UpdateSecret")
    String requestSecret(@RequestBody VaultSecret vaultSecret) {
        return vaultService.updateSecret(vaultSecret.getKey(), vaultSecret.getValue());
    }

    @DeleteMapping("/DeleteSecret/{key}")
    String eraseSecret(@PathVariable String key) {
        return vaultService.deleteSecret(key);
    }
}