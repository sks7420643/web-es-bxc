package ESB.Controller.Security;

import ESB.Repository.Security.Auth.User;
import ESB.Service.Security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/RoleSystem")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PutMapping("/PutUser")
    String putUser(@RequestBody User user) {
        return authService.addUser(user);
    }

    @PutMapping("/UpdatePassword")
    String updatePassword(@RequestBody User user) {
        return authService.updatePassword(user.getUserName(), user.getPassword());
    }

    @PutMapping("/UpdateRole")
    String updateRole(@RequestBody User user) {
        return authService.updateRole(user.getUserName(), user.getRole());
    }

    @PutMapping("/UpdateEnabled")
    String updateEnabled(@RequestBody User user) {
        return authService.updateEnabled(user.getUserName(), user.isEnabled());
    }

    @DeleteMapping("/DeleteUser/{userName}")
    String deleteUser(@RequestParam String userName) {
        return authService.deleteUser(userName);
    }

    @PutMapping("/TestConnection")
    String testConnect(@RequestBody String user) {
        return authService.testConnect(user);
    }

    @GetMapping("/Hi")
    String sayHi() {
        return "Hi";
    }

}
