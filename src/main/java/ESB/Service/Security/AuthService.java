package ESB.Service.Security;

import ESB.Repository.Security.Auth.AuthRepository;
import ESB.Repository.Security.Auth.AuthUserDetails;
import ESB.Repository.Security.Auth.User;
import ESB.RestSender.RestRequestToESB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

@Service
public class AuthService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

    private AuthRepository authRepository;

    private PasswordEncoder encoder;

    private VaultService vault;

    @Autowired
    public AuthService(AuthRepository authRepository, PasswordEncoder encoder, VaultService vault) {
        this.authRepository = authRepository;
        this.encoder = encoder;
        this.vault = vault;
    }

    public AuthService() {
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = authRepository.findByUserName(userName);
        if (user == null) {
            logger.error("User not found in DB");
            throw new UsernameNotFoundException("Could not find user " + userName);
        }
        return new AuthUserDetails(user);
    }

    public String addUser(User user) {
        logger.debug("UserName to add:" + user.getUserName());
        String pass = encoder.encode(user.getPassword());
        user.setPassword(pass);
        authRepository.save(user);
        return "User " + user.getUserName() + " added to repository";
    }

    public String updatePassword(String userName, String password) {
        logger.debug("UserName to upd:" + userName + ". Pass to upd: " + password);
        password = encoder.encode(password);
        int numberOfUpdated = authRepository.updatePasswordByUsername(userName, password);
        return userName + " password updated " + numberOfUpdated + " rows";
    }

    public String updateRole(String userName, String role) {
        logger.debug("UserName to upd:" + userName + ". Role to upd: " + role);
        int numberOfUpdated = authRepository.updateRoleByUsername(userName, role);
        return userName + " role updated " + numberOfUpdated + " rows";
    }

    public String updateEnabled(String userName, boolean enabled) {
        logger.debug("UserName to upd:" + userName + ". Enabled to upd: " + enabled);
        int numberOfUpdated = authRepository.updateEnabledByUsername(userName, enabled);
        return userName + " enabled updated " + numberOfUpdated + " rows";
    }

    public String deleteUser(String userName) {
        logger.debug("UserName to delete:" + userName);
        int numberOfDeleted = authRepository.deleteByUserName(userName);
        return userName + " deleted " + numberOfDeleted + " rows";
    }

    public String testConnect(String user) {
        try {
            RestRequestToESB request = new RestRequestToESB("8443", "OMS01-WSDS-0017.gazprom-neft.local",
                    "https", "RoleSystem/PutUser", "ESBAdmin", vault.getSecret("ESBAdmin"), user);
            return request.makeRequest();
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException |
                 KeyManagementException exception) {
            logger.error("Cant call RoleSys EP: " + exception.getMessage());
        }
        return "ERROR";
    }
}
