package ESB.Service.Security;

import ESB.Repository.Security.Vault.VaultRepository;
import ESB.Repository.Security.Vault.VaultSecret;
import ESB.Utilities.Encryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VaultService {

    private static final Logger logger = LoggerFactory.getLogger(VaultService.class);

    private VaultRepository vaultRepository;

    private Encryptor encryptor;

    @Autowired
    public VaultService(VaultRepository vaultRepository, Encryptor encryptor) {
        this.vaultRepository = vaultRepository;
        this.encryptor = encryptor;
    }

    public VaultService() {
    }

    public String addSecret(VaultSecret vaultSecret) {
        try {
            String value = vaultSecret.getValue();
            vaultSecret.setValue(encryptor.encrypt(value));
            vaultRepository.save(vaultSecret);
        } catch (Exception exception) {
            logger.error("Error adding secret " + vaultSecret.getKey() + " to vault: " + exception.getMessage());
            throw new IllegalArgumentException(exception);
        }
        return "Secret " + vaultSecret.getKey() + " added to repository";
    }

    public String getSecret(String key) {
        try {
            VaultSecret secret = vaultRepository.findByKey(key);
            if (secret.getValue() != null) {
                String encryptedPassword = secret.getValue();
                return encryptor.decrypt(encryptedPassword);
            }
            logger.warn("No secret by " + key + " in vault");
            return "";
        } catch (Exception exception) {
            logger.error("Error getting secret " + key + " from vault: " + exception.getMessage());
            throw new IllegalStateException(exception);
        }
    }

    public String updateSecret(String key, String value) {
        try {
            value = encryptor.encrypt(value);
            int numberOfEdited = vaultRepository.updateValueByKey(key, value);
            return "Secret " + key + " updated. Rows updated " + numberOfEdited;
        } catch (Exception exception) {
            logger.error("Error getting secret " + key + " from vault: " + exception.getMessage());
            throw new IllegalStateException(exception);
        }
    }

    public String deleteSecret(String key) {
        int numberOfDeleted = vaultRepository.deleteByKey(key);
        return "Secret with key " + key + " deleted. Row deleted " + numberOfDeleted;
    }
}


