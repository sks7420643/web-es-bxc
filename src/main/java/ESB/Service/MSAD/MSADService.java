package ESB.Service.MSAD;

import ESB.Repository.MSAD.Employee;
import ESB.Repository.MSAD.MSADRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MSADService {
    @Autowired
    MSADRepository msadRepository;
    public String loadUsersToDB() {

        return "Data loaded!";
    }

    public List<Employee> findEmployeeBySAMAccountName(String name) {
        return msadRepository.findByCanonicalName(name);
    }
}
