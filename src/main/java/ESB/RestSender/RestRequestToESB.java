package ESB.RestSender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class RestRequestToESB extends RestRequestToEndPoint {

    private final String user;

    private final String password;

    private static final Logger logger = LoggerFactory.getLogger(RestRequestToESB.class);

    public RestRequestToESB(String port, String address, String protocol, String method, String user,
                            String password, String message)
            throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {
        super(port, address, protocol, method, message);
        this.user = user;
        this.password = password;
    }

    @Override
    public String makeRequest() {
        addBasicAuth(user, password);
        httpBuilder.header("Content-Type", "application/json");
        httpBuilder.PUT(HttpRequest.BodyPublishers.ofString(message));
        HttpRequest request = httpBuilder.build();
        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            logger.debug("ESB PUT status " + response.statusCode());
            return response.body();
        } catch (IOException | InterruptedException exception) {
            logger.error("Problem sending put request to ESB: " + exception.getMessage());
        }
        return "ERROR";
    }
}
