package ESB.Configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "trust")
public class TrustStore {

    @Getter
    @Setter
    private String store;

    @Getter
    @Setter
    private String password;
}
