package ESB.Repository.MSAD;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;

@Entity
@Table(name = "msadusers")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    @Column(name = "adspath")
    private String ADSPath;
    @Getter
    @Setter
    @Column(name = "displayname")
    private String displayName;
    @Getter
    @Setter
    @Column(name = "cn")
    private String canonicalName;
    @Getter
    @Setter
    @Column(name = "objectsid")
    private String objectSID;
    @Getter
    @Setter
    @Column(name = "useraccountcontrol")
    private String userAccountControl;
    @Getter
    @Setter
    @Column(name = "company")
    private String company;
    @Getter
    @Setter
    @Column(name = "mail")
    private String mail;
    @Getter
    @Setter
    @Column(name = "telephonenumber")
    private String telephoneNumber;
    @Getter
    @Setter
    @Column(name = "extensionattribute7")
    private String extensionAttribute;
    @Getter
    @Setter
    @Column(name = "managerid")
    private String managerID;
    @Getter
    @Setter
    @Column(name = "whencreated")
    private String whenCreated;

    public Employee() {
    }

    public Employee(String ADSPath, String displayName, String canonicalName, String objectSID,
                    String userAccountControl, String company, String mail, String telephoneNumber,
                    String extensionAttribute, String managerID, String whenCreated) {
        this.ADSPath = ADSPath;
        this.displayName = displayName;
        this.canonicalName = canonicalName;
        this.objectSID = objectSID;
        this.userAccountControl = userAccountControl;
        this.company = company;
        this.mail = mail;
        this.telephoneNumber = telephoneNumber;
        this.extensionAttribute = extensionAttribute;
        this.managerID = managerID;
        this.whenCreated = whenCreated;
    }
}
