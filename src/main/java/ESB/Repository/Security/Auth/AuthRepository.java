package ESB.Repository.Security.Auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AuthRepository extends JpaRepository<User, Long> {

    User findByUserName(String name);

    @Modifying
    @Query(value = "update security set password = :password where username = :username", nativeQuery = true)
    int updatePasswordByUsername(@Param(value = "username") String username, @Param(value = "password")String password);

    @Modifying
    @Query(value = "update security set role = :role where username = :username", nativeQuery = true)
    int updateRoleByUsername(@Param(value = "username") String username, @Param(value = "role") String role);

    @Modifying
    @Query(value = "update security set enabled = :enabled where username = :username", nativeQuery = true)
    int updateEnabledByUsername(@Param(value = "username") String username, @Param(value = "enabled") boolean enabled);

    int deleteByUserName(String name);
}
