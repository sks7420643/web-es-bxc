package ESB.Repository.Security.Auth;

import ESB.Configuration.Role;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "security")
public class User {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name = "username", unique = true)
    private String userName = "defaultname";
    @Getter
    @Setter
    @Column(name = "password")
    private String password = "defaultpass";
    @Getter
    @Setter
    @Column(name = "role")
    private String role = Role.USER.name;
    @Getter
    @Setter
    @Column(name = "enabled")
    private boolean enabled = true;

    public User() {}

    public User(String userName, String password, String role, boolean enabled) {
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
    }
}
