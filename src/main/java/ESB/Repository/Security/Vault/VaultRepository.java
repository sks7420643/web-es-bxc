package ESB.Repository.Security.Vault;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface VaultRepository extends JpaRepository<VaultSecret, Long> {
    VaultSecret findByKey(String key);

    @Modifying
    @Query(value = "update vault set value = :value where key = :key", nativeQuery = true)
    int updateValueByKey(@Param(value = "key") String key, @Param(value = "value")String value);

    int deleteByKey(String key);
}
